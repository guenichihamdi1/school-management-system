package dao;

import java.util.List;

import metier.entities.Etudiant;

public interface IEtudiantDao {
    public Etudiant save(Etudiant p);

    public List<Etudiant> etudiantsParMC(String mc);

}
