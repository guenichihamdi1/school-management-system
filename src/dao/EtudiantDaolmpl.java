package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import metier.SingletonConnection;
import metier.entities.Etudiant; 

public class EtudiantDaolmpl implements IEtudiantDao {

    @Override
    public Etudiant save(Etudiant p) {
        Connection conn = SingletonConnection.getConnection();
        try {
            PreparedStatement ps = conn.prepareStatement("INSERT INTO Etudiant(nom,prenom,datenais,sexe,idClasse) VALUES(?,?,?,?,?)");
            ps.setString(1, p.getnom());
            ps.setString(1, p.getprenom());
            ps.setString(1, p.getdatenais());
            ps.setString(1, p.getsexe());
            ps.setLong(1, p.getIdClasse());
            ps.executeUpdate();

        } catch (SQLException e) {

            e.printStackTrace();
        }
        return p;
    }

    @Override
    public List<Etudiant> etudiantsParMC(String mc) {
        List<Etudiant> prods = new ArrayList<Etudiant>();
        Connection conn = SingletonConnection.getConnection();
        try {
            PreparedStatement ps = conn.prepareStatement("select * from etudiant where nom LIKE ?");
            ps.setString(1, "%" + mc + "%");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Etudiant p = new Etudiant();
                prods.add(p);
            }

        } catch (SQLException e) {

            e.printStackTrace();
        }

        return prods;
    }
}
