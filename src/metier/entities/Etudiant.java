package metier.entities;

import java.io.Serializable;

public class Etudiant implements Serializable {
    private Long idClasse;
    private String nom;
    private String prenom;
    private String sexe;
    private String datenais;

    public Etudiant() {
        super();
    }

    public Etudiant(String nom, String prenom, String sexe, String datenais, Long idClasse) {
        super();
        this.nom = nom;
        this.prenom = prenom;
        this.sexe = sexe;
        this.datenais = datenais;
        this.idClasse = idClasse;
    }

    public Long getIdClasse() {
        return idClasse;
    }

    public void setIdClasse(Long idClasse) {
        this.idClasse = idClasse;
    }    

    public String getnom() {
        return nom;
    }

    public void setnom(String nom) {
        this.nom = nom;
    }

    public String getprenom() {
        return prenom;
    }

    public void setprenom(String prenom) {
        this.prenom = prenom;
    }

    public String getsexe() {
        return sexe;
    }

    public void setsexe(String sexe) {
        this.sexe = sexe;
    }

    public String getdatenais() {
        return datenais;
    }

    public void setdatenais(String datenais) {
        this.datenais = datenais;
    }

    @Override
    public String toString() {
        return "Etudiant [nom=" + nom + ", prenom=" + prenom + ", datenais=" + datenais + ", sexe=" + sexe +", idClasse=" + idClasse + "]";
    }

}